﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD
{
    class Program2
    {
        static void Main2(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();
            //Lista de funcionarios que trabalham em DF

            //  var lista = from Funcionario in dc.Funcionarios where Funcionario.Departamento == "DF" select Funcionario;
            //     var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.StartsWith("Jo") select Funcionario;
            var lista = from Funcionario in dc.Funcionarios where Funcionario.Nome.Contains("ana")    select Funcionario;

            foreach (var item in lista)
            {
                Console.Write("ID: " + item.ID);
                Console.Write(" Nome: " + item.Nome);
                Console.Write(" Departamento: " + item.Departamento);

                Console.WriteLine();

            }
            Console.WriteLine("Existem de momento {0} funcionários", lista.Count());


            //Agrupar informação - Contar funcionários por departamento

            var novalista = from Funcionario in dc.Funcionarios
                            group Funcionario by Funcionario.Departamento
                            into c /* where c.Count() > 3 */
                            select new
                            {
                                Departamento = c.Key, Contagem = c.Count()
                            };
            foreach (var c in novalista)
            {
                Console.WriteLine(c.Departamento  +  "(" + c.Contagem + ")");
            }
            //    dc.ExecuteCommand(@"INSERT INTO Funcionarios (ID, Nome, Departamento) VALUES(10, 'Ana maria', 'DC'); ");
            //    dc.ExecuteCommand(@"DELETE FROM  Funcionarios WHERE ID = 10; ");

            Console.WriteLine("---------Junção de tabelas---------------------------");
            var outraLista = from Funcionario in dc.Funcionarios
                             join Departamento in dc.Departamentos
                             on Funcionario.Departamento equals Departamento.Sigla
                             select new
                             {
                                 Funcionario.ID,
                                 Funcionario.Nome,
                                 Departamento.Departamento1
                             };

            foreach (var c in outraLista)
            {
                Console.WriteLine("ID: " + c.ID);
                Console.WriteLine("Nome: " + c.Nome);
                Console.WriteLine("Departamento: " + c.Departamento1);

                Console.WriteLine();
            }


            Console.ReadKey();
           
        }
    }
}
