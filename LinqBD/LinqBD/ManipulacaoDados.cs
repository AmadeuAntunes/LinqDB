﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBD
{
    class ManipulacaoDados
    {
        static void Main(string[] args)
        {
            DataClasses1DataContext dc = new DataClasses1DataContext();
            //carregar lista de funcionários

            Funcionario fuc = new Funcionario
            {
                ID = dc.Funcionarios.Count() + 10,
                Nome = "Carlos Xavier",
                Departamento = "DF",
            };
           
            dc.Funcionarios.InsertOnSubmit(fuc);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }

            var Lista = from Funcionario in dc.Funcionarios select Funcionario;
            foreach (var c in Lista)
            {
                Console.WriteLine("ID: " + c.ID);
                Console.WriteLine("Nome: " + c.Nome);
                Console.WriteLine("Departamento: " + c.Departamento);

                Console.WriteLine();
            }


            //Alterar
            Funcionario funcionarioAlterar = new Funcionario();

            var pesquisar = from Funcionario in dc.Funcionarios
                            where Funcionario.ID == 4
                            select Funcionario;

            funcionarioAlterar = pesquisar.Single();
            funcionarioAlterar.Departamento = "RH";


            try
            {
                dc.SubmitChanges();
               
            }
            catch (Exception e )
            {
                Console.WriteLine(e.Message);
            }


            var Listaalterada = from Funcionario in dc.Funcionarios select Funcionario;
            foreach (var c in Listaalterada)
            {
                Console.WriteLine("ID: " + c.ID);
                Console.WriteLine("Nome: " + c.Nome);
                Console.WriteLine("Departamento: " + c.Departamento);

                Console.WriteLine();
            }

            Console.WriteLine("------------Eliminar-----------------");

            Funcionario f = new Funcionario();
            var outraPesquisa = from Funcionario in dc.Funcionarios
                                where Funcionario.Nome == "Carlos Xavier"
                                select Funcionario;
            foreach (var x in outraPesquisa)
            {
                dc.Funcionarios.DeleteOnSubmit(x);
            }
         


            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
            }
            var Listaapagada = from Funcionario in dc.Funcionarios select Funcionario;
            foreach (var c in Listaapagada)
            {
                Console.WriteLine("ID: " + c.ID);
                Console.WriteLine("Nome: " + c.Nome);
                Console.WriteLine("Departamento: " + c.Departamento);

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
