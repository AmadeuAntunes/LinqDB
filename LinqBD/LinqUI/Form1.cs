﻿using LinqBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LinqUI
{
    public partial class Form1 : Form
    {

        private void ListViewInitialize()
        {
            listView1.Columns.Add("Id");
            listView1.Columns.Add("Nome");
            listView1.Columns.Add("Departamento");
        }

        private void ListViewAutoResize(IQueryable<Funcionario> lista)
        {

            foreach (Funcionario fuc in lista)
            {
                ListViewItem item;
                item = listView1.Items.Add(fuc.ID.ToString());
                item.SubItems.Add(fuc.Nome);
                item.SubItems.Add(fuc.Departamento);
            }
            for (int i = 0; i <= listView1.Columns.Count - 1; i++)
            {
                listView1.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
        }
        DataClasses1DataContext dc = new DataClasses1DataContext();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //ListView
            ListViewInitialize();
            //Carregar Dados
            var lista = from Funcionario in dc.Funcionarios select Funcionario;

            ListViewAutoResize(lista);

            //TreView

            var outraLista = from Departamento in dc.Departamentos
                             select Departamento;

            foreach (var dep in outraLista)
            {
                treeView1.Nodes.Add(dep.Sigla);
            }

            //segundo nivel da árvore - Funcionários
            var outraLista2 = from Funcionario in dc.Funcionarios
                              orderby Funcionario.Nome
                              select Funcionario;

            string departamento;
            foreach (Funcionario func in outraLista2)
            {
                departamento = func.Departamento;
                foreach (TreeNode principal in treeView1.Nodes)
                {
                    if(principal.Text == departamento)
                    {
                        principal.Nodes.Add(func.Nome);
                    }
                }
            }
            //gridview
            dataGridView1.Columns.Add("ColID", "ID");
            dataGridView1.Columns.Add("ColNome", "Nome");
            dataGridView1.Columns.Add("ColDepartamento", "Departamento");

            var outralista3 = from Funcionario in dc.Funcionarios select Funcionario;
            int idxlinha = 0;

            DataGridViewCellStyle vermelho = new DataGridViewCellStyle();
            vermelho.ForeColor = Color.Red;

           
            foreach (Funcionario func in outralista3)
            {
                DataGridViewRow linha = new DataGridViewRow();
                dataGridView1.Rows.Add(linha);
              
                dataGridView1.Rows[idxlinha].Cells[0].Value = func.ID;
                dataGridView1.Rows[idxlinha].Cells[1].Value = func.Nome;
                dataGridView1.Rows[idxlinha].Cells[2].Value = func.Departamento;

                if ((string)dataGridView1.Rows[idxlinha].Cells[2].Value == "DC")
                {
                    dataGridView1.Rows[idxlinha].DefaultCellStyle = vermelho;
                }
                idxlinha++;
               // dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            }
        }

        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            listView1.Clear();
            ListViewInitialize();
            IOrderedQueryable<Funcionario> lista = null;
            switch (e.Column)
            {
                case 0:
                    {
                        lista  = from Funcionario in dc.Funcionarios
                                    orderby Funcionario.ID
                                    select Funcionario;
                        break;
                    }
                case 1:
                    {
                       lista = from Funcionario in dc.Funcionarios
                                    orderby Funcionario.Nome
                                    select Funcionario;
                        break;
                    }
                case 2:
                    {
                        lista = from Funcionario in dc.Funcionarios
                                orderby Funcionario.Departamento
                                select Funcionario;
                        break;
                    }
            }
            ListViewAutoResize(lista);



        }
    }
}
